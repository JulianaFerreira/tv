-- Strings (Inicio)
local btDetalhes = "Detalhes";
local btResultado = "Resultado";
local btSair = "Sair";
local tvTituloPagina = "Confrontos";
local tvPlacar = "1X1";

-- Strings (Fim)

-- Doubles (Inicio) --
local marginTop = 10;
local marginLeft = 10;
local alturaBts = 30;
local larguraBts = 30;
local larguraImgTime = 45;
local alturaImgTime = 45;
-- Doubles (Fim)

-- Tabelas (Inicio) --
-- Tabelas (Fim) --

-- Regi�o T�tulo (Inicio) --
canvas:attrFont("Tiresias",48,"bold");

canvas:attrColor(0,0,255,255); -- BLUE
canvas:drawRect("fill",0,0,canvas:attrSize());

canvas:attrColor(0,0,0,255); -- BLACK
canvas:drawText(marginLeft,0,tvTituloPagina);
-- Regiao Titulo (Fim)

-- Regiao Placar (Inicio)
local larguraTituloPagina,alturaTituloPagina = canvas:measureText(tvTituloPagina);
marginTop = alturaTituloPagina + marginTop

canvas:attrFont("Tiresias",24,"bold");
canvas:drawText(marginLeft + larguraImgTime,marginTop + 5,tvPlacar);
local larguraTvPlacar, alturaTvPlacar = canvas:measureText(tvPlacar);
marginTop = alturaImgTime + 5 + marginTop;
canvas:drawText(marginLeft + larguraImgTime,marginTop + 5,tvPlacar);
-- Regiao Placar (Fim)

-- Regiao Botoes (Inicio)
marginTop = marginTop + alturaImgTime + 50;

local imgBtDetalhes = canvas:new("../medias/legenda_ok_30.png");
canvas:compose(marginLeft, marginTop, imgBtDetalhes);
canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btDetalhes);

marginTop = marginTop + 10 + alturaBts;
local imgBtResultado = canvas:new("../medias/legenda_amarelo_30.png");
canvas:compose(marginLeft, marginTop, imgBtResultado);
canvas:drawText(marginLeft + larguraBts + 5, marginTop, btResultado);

marginTop = marginTop + 10 + alturaBts;
local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
canvas:compose(marginLeft, marginTop, imgBtSair);
canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
-- Regiao Botoes (Fim)

-- Flush
canvas:flush();
