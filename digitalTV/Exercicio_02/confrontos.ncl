<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="nclua" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<regionBase>
			<region id="regiaoGeral" width="400px" height="400px">
				<region id="btSportReg" left="2.5%" width="45" height="45" top="16%" zIndex="2"/>
				<region id="btSantosReg" left="2.5%" width="45" height="45" top="29%" zIndex="2"/>
				<region id="btFlamengoReg" left="24%" width="45" height="45" top="16%" zIndex="2"/>
				<region id="btVascoReg" left="24%" width="45" height="45" top="29%" zIndex="2"/>
			</region>
				
		</regionBase>
		<descriptorBase>
			<descriptor id="descritorGeral" region="regiaoGeral"/>
			<descriptor id="btSportDesc" region="btSportReg" focusIndex="1" moveUp="3" moveDown="3" moveLeft="2" moveRight="2" focusBorderColor="yellow"/>
			<descriptor id="btFlamengoDesc" region="btFlamengoReg" focusIndex="2" moveUp="4" moveDown="4" moveLeft="1" moveRight="1" focusBorderColor="yellow"/>
			<descriptor id="btSantosDesc" region="btSantosReg" focusIndex="3" moveUp="1" moveDown="1" moveLeft="4" moveRight="4" focusBorderColor="yellow"/>
			<descriptor id="btVascoDesc" region="btVascoReg" focusIndex="4" moveUp="2" moveDown="2" moveLeft="3" moveRight="3" focusBorderColor="yellow"/>
		</descriptorBase>
	</head>
	<body>
		<port id="port01" component="media01"/>
		<media id="media01" descriptor="descritorGeral" src="lua/confrontos.lua" type="application/x-ginga-NCLua"/>
		<port id="pTimeSport" component="mediaSport"/>
		<media id="mediaSport" src="medias/times/sport_45.png" descriptor="btSportDesc"/>
		<port id="pTimeSantos" component="mediaSantos"/>
		<media id="mediaSantos" src="medias/times/santos_45x45.png" descriptor="btSantosDesc"/>
		<port id="pTimeFlamengo" component="mediaFlamengo"/>
		<media id="mediaFlamengo" src="medias/times/flamengo_45x45.png" descriptor="btFlamengoDesc"/>
		<port id="pTimeVasco" component="mediaVasco"/>
		<media id="mediaVasco" src="medias/times/vasco_45x45.png" descriptor="btVascoDesc"/>
	</body>
</ncl>
