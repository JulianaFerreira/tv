-- Strings (Inicio)
local lblEstadio = "Est�dio: ";
local lblElenco = "Elenco: ";
local lblTitulos = "T�tulos: ";
local btConfrontos = "Confrontos";
local btResultado = "Resultado";
local btSair = "Sair";
local tvTituloPagina = "Detalhes";
local tvEstadio = "Nome do est�dio";
local tvCapacidade = "(capacidade)"
local tvElenco = "Lista de jogadores";
local tvTitulos = "Lista de t�tulos";
-- Strings (Fim)

-- Doubles (Inicio) --
local marginTop = 10;
local marginLeft = 10;
local alturaBts = 30;
local larguraBts = 30;
-- Doubles (Fim)

-- Tabelas (Inicio) --
local tableRegiaoDetalhes = {
  lblEstadio..tvEstadio,
  tvCapacidade,
  lblElenco..tvElenco,
  lblTitulos..tvTitulos};
-- Tabelas (Fim) --

-- Regi�o T�tulo (Inicio) --
canvas:attrFont("Tiresias",48,"bold");

canvas:attrColor(0,0,255,255); -- BLUE
canvas:drawRect("fill",0,0,canvas:attrSize());

canvas:attrColor(0,0,0,255); -- BLACK
canvas:drawText(marginLeft,0,tvTituloPagina);

local larguraTituloPagina,alturaTituloPagina = canvas:measureText(tvTituloPagina);
local imgSport = canvas:new("../medias/times/sport_45.png");

marginTop = alturaTituloPagina + marginTop
canvas:compose (marginLeft,marginTop,imgSport);
local larguraImgSport,alturaImgSport = imgSport:attrSize();

-- Regiao Titulo (Fim)

-- Regiao Detalhes (Inicio)
canvas:attrFont("Tiresias",24,"bold");
local larguraTvEstadio, alturaTvEstadio = canvas:measureText(tvEstadio);

marginTop = marginTop + alturaImgSport + 10;
for index,value in pairs(tableRegiaoDetalhes) do
  canvas:drawText(marginLeft,marginTop,tostring(value));
  marginTop = marginTop + alturaTvEstadio;
end
-- Regiao Detalhes (Fim)

-- Regiao Botoes (Inicio)
marginTop = marginTop + 50;

local imgBtConfrontos = canvas:new("../medias/legenda_azul_30.png");
canvas:compose(marginLeft, marginTop, imgBtConfrontos);
canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btConfrontos);

marginTop = marginTop + 10 + alturaBts;
local imgBtResultado = canvas:new("../medias/legenda_amarelo_30.png");
canvas:compose(marginLeft, marginTop, imgBtResultado);
canvas:drawText(marginLeft + larguraBts + 5, marginTop, btResultado);

marginTop = marginTop + 10 + alturaBts;
local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
canvas:compose(marginLeft, marginTop, imgBtSair);
canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
-- Regiao Botoes (Fim)

-- Flush
canvas:flush();
