function desenharTabelaClassificacao(numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna)

  -- cor da tabela
  canvas:attrColor(0,255,255,255);
  canvas:drawRect("fill", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);

  -- cor das linhas da tabela
  canvas:attrColor(255,255,255,255); -- WHITE
  canvas:drawRect("frame", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);
  for i=1,numeroLinhas,1 do
    -- linha horizontal
    canvas:drawLine(marginLeftColuna, marginTopColuna + tamanhoColuna*i, marginLeftColuna + numeroColunas*tamanhoColuna, marginTopColuna+tamanhoColuna*i);
  end

  for i=1, numeroColunas,1 do
    -- linha vertical
    canvas:drawLine(marginLeftColuna + tamanhoColuna*i, marginTopColuna, marginLeftColuna + tamanhoColuna*i, marginTopColuna + numeroLinhas * tamanhoColuna);
  end
end

function preencherTabelaClassificacao(tableClassificacao,numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna, paddingLeft)

  canvas:attrColor(0,0,0,255);
  canvas:attrFont("Tiresias",20,"bold");

  for i=1,numeroColunas,1 do
    canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*(i-1), marginTopColuna+paddingLeft, tableClassificacao[1][i]);
  end

  for i=2, numeroLinhas,1 do
    canvas:compose (marginLeftColuna+paddingLeft, marginTopColuna+paddingLeft+tamanhoColuna*(i-1), canvas:new(tableClassificacao[i][1]));
    for j=1, numeroColunas-1,1 do
      canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*j,marginTopColuna+paddingLeft+tamanhoColuna*(i-1),tableClassificacao[i][j+1]);
    end 
  end
end