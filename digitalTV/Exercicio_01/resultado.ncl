<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="nclua" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<regionBase>
			<region id="regiaoGeral" width="400px" height="460px"/>
		</regionBase>
		<descriptorBase>
			<descriptor id="descritorGeral" region="regiaoGeral"/>
		</descriptorBase>
	</head>
	<body>
		<port id="port01" component="media01"/>
		<media id="media01" descriptor="descritorGeral" src="lua/resultado.lua" type="application/x-ginga-NCLua"/>
	</body>
</ncl>
