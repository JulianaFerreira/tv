<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="nclua" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<transitionBase>
			<transition id="transicaoFade" type="fade" dur="2s"/>
		</transitionBase>
		<regionBase>
			<region id="regiaoGeral" width="400px" height="460px" zIndex="1">
				<region id="btSportReg" left="2.5%" width="45" height="45" top="16%" zIndex="2"/>
				<region id="btSantosReg" left="2.5%" width="45" height="45" top="29%" zIndex="2"/>
				<region id="btFlamengoReg" left="24%" width="45" height="45" top="16%" zIndex="2"/>
				<region id="btVascoReg" left="24%" width="45" height="45" top="29%" zIndex="2"/>
				<region id="inputRodadaReg" left="2.5%" width="200px" height="30px" top="40%" zIndex="2"/>
			</region>
		</regionBase>
		<descriptorBase>
			<descriptor id="descritorGeral" region="regiaoGeral" transIn="transicaoFade"/>
			<descriptor id="btPrimeiroTimeDesc" region="btSportReg" focusIndex="1" moveUp="3" moveDown="3" moveLeft="2" moveRight="2" focusBorderColor="yellow" transIn="transicaoFade"/>
			<descriptor id="btSegundoTimeDesc" region="btFlamengoReg" focusIndex="2" moveUp="4" moveDown="4" moveLeft="1" moveRight="1" focusBorderColor="yellow" transIn="transicaoFade"/>
			<descriptor id="btTerceiroTimeDesc" region="btSantosReg" focusIndex="3" moveUp="1" moveDown="5" moveLeft="4" moveRight="4" focusBorderColor="yellow" transIn="transicaoFade"/>
			<descriptor id="btQuartoTimeDesc" region="btVascoReg" focusIndex="4" moveUp="2" moveDown="5" moveLeft="3" moveRight="3" focusBorderColor="yellow" transIn="transicaoFade"/>
			<descriptor id="inputRodadaDesc" region="inputRodadaReg" moveUp="3" focusIndex="5" focusBorderColor="yellow" transIn="transicaoFade"/>
		</descriptorBase>
		<connectorBase>
			<importBase alias="con" documentURI="conectores.ncl"/>
		</connectorBase>
	</head>
	<body>
		<!-- Telas (In�cio) -->
		<port id="pConfrontos" component="mConfrontos"/>
		<media id="mConfrontos" descriptor="descritorGeral" src="lua/confrontos.lua" type="application/x-ginga-NCLua">
			<property name="rodadaSelecionada"/>
		</media>
		<media id="mDetalhes" descriptor="descritorGeral" src="lua/detalhes.lua" type="application/x-ginga-NCLua">
			<property name="timeSelecionado"/>
		</media>
		<media id="mResultado" descriptor="descritorGeral" src="lua/resultado.lua" type="application/x-ginga-NCLua"/>
		<!-- Telas (Fim) -->
		<!-- Times (In�cio) -->
		<port id="pTimeSport" component="mTimeSport"/>
		<media id="mTimeSport" src="medias/times/sport_45.png" descriptor="btPrimeiroTimeDesc"/>
		<port id="pTimeSantos" component="mTimeSantos"/>
		<media id="mTimeSantos" src="medias/times/santos_45x45.png" descriptor="btTerceiroTimeDesc"/>
		<port id="pTimeFlamengo" component="mTimeFlamengo"/>
		<media id="mTimeFlamengo" src="medias/times/flamengo_45x45.png" descriptor="btSegundoTimeDesc"/>
		<port id="pTimeVasco" component="mTimeVasco"/>
		<media id="mTimeVasco" src="medias/times/vasco_45x45.png" descriptor="btQuartoTimeDesc"/>
		<media id="mTimeSport2" src="medias/times/sport_45.png" descriptor="btSegundoTimeDesc"/>
		<media id="mTimeSantos2" src="medias/times/santos_45x45.png" descriptor="btPrimeiroTimeDesc"/>
		<media id="mTimeFlamengo2" src="medias/times/flamengo_45x45.png" descriptor="btQuartoTimeDesc"/>
		<media id="mTimeVasco2" src="medias/times/vasco_45x45.png" descriptor="btTerceiroTimeDesc"/>
		<!-- Times (Fim) -->
		<!-- Settings (In�cio) -->
		<media type="application/x-ginga-settings" id="programSettings">
			<property name="channel.keyCapture" value="true"/>
		</media>
		<port id="pInput" component="mInput"/>
		<media id="mInput" src="lua/input.lua" descriptor="inputRodadaDesc">
			<property name="text"/>
		</media>
		<!-- Settings (Fim) -->
		<!-- Navega��o (In�cio) -->
		<!-- Tela Confrontos (In�cio) -->
		<link xconnector="con#naSelecaoDeTeclaPararIniciar">
			<bind role="onSelection" component="mConfrontos">
				<bindParam name="tecla" value="YELLOW"/>
			</bind>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mInput"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="start" component="mResultado"/>
		</link>
		<link xconnector="con#naSelecaoDeTeclaParar">
			<bind role="onSelection" component="mConfrontos">
				<bindParam name="tecla" value="RED"/>
			</bind>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mInput"/>
			<bind role="stop" component="mConfrontos"/>
		</link>
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeSport"/>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeSport"/>
			</bind>
		</link>
			<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeSport2"/>
			<bind role="stop" component="mTimeSport2"/>
			<bind role="stop" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeVasco2"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeSport"/>
			</bind>
		</link>
		
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeSantos"/>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeSantos"/>
			</bind>
		</link>
		
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeSport2"/>
			<bind role="stop" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeVasco2"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeSantos"/>
			</bind>
		</link>
		
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeFlamengo"/>
			</bind>
		</link>
		
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeSport2"/>
			<bind role="stop" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeVasco2"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeFlamengo"/>
			</bind>
		</link>
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeVasco"/>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeVasco"/>
			</bind>
		</link>
		
		<link xconnector="con#naSelecaoSetarPararIniciar">
			<bind role="onSelection" component="mTimeVasco2"/>
			<bind role="stop" component="mTimeSport2"/>
			<bind role="stop" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeVasco2"/>
			<bind role="stop" component="mConfrontos"/>
			<bind role="stop" component="mInput"/>
			<bind role="start" component="mDetalhes"/>
			<bind role="set" component="mDetalhes" interface="timeSelecionado">
				<bindParam name="var" value="timeVasco"/>
			</bind>
		</link>
		
		<link xconnector="con#onEndAttributionStopStart">
			<linkParam name="var" value="2"/>
			<bind role="onEndAttribution" component="mInput" interface="text"/>
			<bind role="prop" component="mInput" interface="text"/>
			<bind role="stop" component="mTimeSport"/>
			<bind role="stop" component="mTimeFlamengo"/>
			<bind role="stop" component="mTimeSantos"/>
			<bind role="stop" component="mTimeVasco"/>
			<bind role="start" component="mTimeSport2"/>
			<bind role="start" component="mTimeFlamengo2"/>
			<bind role="start" component="mTimeSantos2"/>
			<bind role="start" component="mTimeVasco2"/>
			<bind role="set" component="mConfrontos" interface="rodadaSelecionada">
				<bindParam name="var" value="2"/>
			</bind>
		</link>
		<link xconnector="con#onEndAttributionStopStart">
			<linkParam name="var" value="1"/>
			<bind role="onEndAttribution" component="mInput" interface="text"/>
			<bind role="prop" component="mInput" interface="text"/>
			<bind role="stop" component="mTimeSport2"/>
			<bind role="stop" component="mTimeFlamengo2"/>
			<bind role="stop" component="mTimeSantos2"/>
			<bind role="stop" component="mTimeVasco2"/>
			<bind role="start" component="mTimeSport"/>
			<bind role="start" component="mTimeFlamengo"/>
			<bind role="start" component="mTimeSantos"/>
			<bind role="start" component="mTimeVasco"/>
			<bind role="set" component="mConfrontos" interface="rodadaSelecionada">
				<bindParam name="var" value="1"/>
			</bind>
		</link>
		<!-- Tela Confrontos (Fim) -->
		<!-- Tela Resultado (In�cio) -->
		<link xconnector="con#naSelecaoDeTeclaPararIniciar">
			<bind role="onSelection" component="mResultado">
				<bindParam name="tecla" value="BLUE"/>
			</bind>
			<bind role="stop" component="mResultado"/>
			<bind role="start" component="mTimeSport"/>
			<bind role="start" component="mTimeSantos"/>
			<bind role="start" component="mTimeFlamengo"/>
			<bind role="start" component="mTimeVasco"/>
			<bind role="start" component="mInput"/>
			<bind role="start" component="mConfrontos"/>
		</link>
		<link xconnector="con#naSelecaoDeTeclaParar">
			<bind role="onSelection" component="mResultado">
				<bindParam name="tecla" value="RED"/>
			</bind>
			<bind role="stop" component="mResultado"/>
		</link>
		<!-- Tela Resultado (Fim) -->
		<!-- Tela Detalhes (In�cio) -->
		<link xconnector="con#naSelecaoDeTeclaPararIniciar">
			<bind role="onSelection" component="mDetalhes">
				<bindParam name="tecla" value="BLUE"/>
			</bind>
			<bind role="stop" component="mDetalhes"/>
			<bind role="start" component="mTimeSport"/>
			<bind role="start" component="mTimeSantos"/>
			<bind role="start" component="mTimeFlamengo"/>
			<bind role="start" component="mTimeVasco"/>
			<bind role="start" component="mInput"/>
			<bind role="start" component="mConfrontos"/>
		</link>
		<link xconnector="con#naSelecaoDeTeclaPararIniciar">
			<bind role="onSelection" component="mDetalhes">
				<bindParam name="tecla" value="YELLOW"/>
			</bind>
			<bind role="stop" component="mDetalhes"/>
			<bind role="start" component="mResultado"/>
		</link>
		<link xconnector="con#naSelecaoDeTeclaParar">
			<bind role="onSelection" component="mDetalhes">
				<bindParam name="tecla" value="RED"/>
			</bind>
			<bind role="stop" component="mDetalhes"/>
		</link>
		<!-- Tela Detalhes (Fim) -->
		<!-- Navega��o (Fim) -->
		<!-- Inicializar -->
	</body>
</ncl>
