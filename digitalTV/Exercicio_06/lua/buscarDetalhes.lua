dofile("tcp.lua")
dofile("Webs.lua")

tabelaDetalhes = {};

local function extrairDados(str)
  tabelaDetalhes[1] = string.match(str,"<nome_estadio>(.*)</nome_estadio>")
  tabelaDetalhes[2] = string.match(str,"<capacidade>(.*)</capacidade>")
  tabelaDetalhes[3] = string.match(str,"<elenco>(.*)</elenco>")
  tabelaDetalhes[4] = string.match(str,"<titulos>(.*)</titulos>")
  return tabelaDetalhes
end

function buscarDetalhes(url, f)
  local resultado = {}
  local status = nil
  local APP = coroutine.create (
    function ()
      print('*** Localizando o detalhes do Time: ')
      local dados = webs_get(url,{})
      print(dados);

      resultado = extrairDados(dados)

      if (tabelaDetalhes[1] ~= nil) then
        print("Detalhes do time Encontrado!!");
        status = 1;
      else
        print("Detalhes do time n�o Encontrado!!");
        status = -1;

      end
      f(resultado,status)
    end
  )
  coroutine.resume(APP)
end








