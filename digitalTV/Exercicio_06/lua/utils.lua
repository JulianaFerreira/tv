dofile("tcp.lua")
dofile("Webs.lua")

function desenharTabelaClassificacao(numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna)

  -- cor da tabela
  canvas:attrColor(0,255,255,255);
  canvas:drawRect("fill", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);

  -- cor das linhas da tabela
  canvas:attrColor(255,255,255,255); -- WHITE
  canvas:drawRect("frame", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);
  for i=1,numeroLinhas,1 do
    -- linha horizontal
    canvas:drawLine(marginLeftColuna, marginTopColuna + tamanhoColuna*i, marginLeftColuna + numeroColunas*tamanhoColuna, marginTopColuna+tamanhoColuna*i);
  end

  for i=1, numeroColunas,1 do
    -- linha vertical
    canvas:drawLine(marginLeftColuna + tamanhoColuna*i, marginTopColuna, marginLeftColuna + tamanhoColuna*i, marginTopColuna + numeroLinhas * tamanhoColuna);
  end
end

function preencherTabelaClassificacao(tableClassificacao,numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna, paddingLeft)

  canvas:attrColor(0,0,0,255);
  canvas:attrFont("Tiresias",20,"bold");

  for i=1,numeroColunas,1 do
    canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*(i-1), marginTopColuna+paddingLeft, tableClassificacao[1][i]);
  end

  for i=2, numeroLinhas,1 do
    canvas:compose (marginLeftColuna+paddingLeft, marginTopColuna+paddingLeft+tamanhoColuna*(i-1), canvas:new(tableClassificacao[i][1]));
    for j=1, numeroColunas-1,1 do
      canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*j,marginTopColuna+paddingLeft+tamanhoColuna*(i-1),tableClassificacao[i][j+1]);
    end
  end
end

function desenharDetalhes(nomeTime,srcImgTime,arquivoTime,linkArquivo)
  nomeTimeGlobal = nomeTime;
  srcImgTimeGlobal = srcImgTime;
  arquivoTimeGlobal = arquivoTime;
  getTabelaArquivoNuvem(linkArquivo)
end

local function desenharDetalhes(tabelaArquivo, status)
  local lblEstadio = "Est�dio: ";
  local lblElenco = "Elenco: ";
  local lblTitulos = "T�tulos: ";
  local btConfrontos = "Confrontos";
  local btResultado = "Resultado";
  local btSair = "Sair";
  local tvTituloPagina = "Detalhes";
  local tvEstadio = tabelaArquivo[1]
  local tvCapacidade = tabelaArquivo[2]
  local tvElenco = tabelaArquivo[3]
  local tvTitulos = tabelaArquivo[4]
  -- Strings (Fim)

  -- Doubles (Inicio) --
  local marginTop = 10;

  local marginLeft = 10;
  local alturaBts = 30;
  local larguraBts = 30;
  local finalPosicaoEscudo = 165;
  -- Doubles (Fim)

  -- Tabelas (Inicio) --
  local tableRegiaoDetalhes = {
    lblEstadio..tvEstadio,
    tvCapacidade,
    lblElenco..tvElenco,
    lblTitulos..tvTitulos};
  -- Tabelas (Fim) --

  -- Regi�o T�tulo (Inicio) --
  canvas:attrFont("Tiresias",48,"bold");

  canvas:attrColor(0,0,255,255); -- BLUE
  canvas:drawRect("fill",0,0,canvas:attrSize());

  canvas:attrColor(0,0,0,255); -- BLACK
  canvas:drawText(marginLeft,0,tvTituloPagina);

  local larguraTituloPagina,alturaTituloPagina = canvas:measureText(tvTituloPagina);
  local imgTime = canvas:new(srcImgTimeGlobal);

  marginTop = alturaTituloPagina + marginTop;
  local marginTopEscudo = marginTop;
  event.timer(2000,
    function()
      deslocarEscudo(marginLeft, marginTopEscudo, imgTime, finalPosicaoEscudo)
    end
  )


  local larguraimgTime,alturaimgTime = imgTime:attrSize();

  -- Regiao Titulo (Fim)

  -- Regiao Detalhes (Inicio)
  canvas:attrColor(0,0,0,255); -- BLACK
  canvas:attrFont("Tiresias",18,"bold");
  local larguraTvEstadio, alturaTvEstadio = canvas:measureText(tvEstadio);

  marginTop = marginTop + alturaimgTime + 10;
  for index,value in pairs(tableRegiaoDetalhes) do
    canvas:drawText(marginLeft,marginTop,tostring(value));
    marginTop = marginTop + alturaTvEstadio;
  end


  if(status == -1) then
    canvas:attrColor(255,0,0,255); -- RED
    canvas:drawText(marginLeft,marginTop+10,"OBS: Dados podem estar desatualizados!");
    canvas:attrColor(0,0,0,255); -- BLACK
  end
  -- Regiao Detalhes (Fim)

  -- Regiao Botoes (Inicio)
  marginTop = marginTop + 50;

  local imgBtConfrontos = canvas:new("../medias/legenda_azul_30.png");
  canvas:compose(marginLeft, marginTop, imgBtConfrontos);
  canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btConfrontos);

  marginTop = marginTop + 10 + alturaBts;
  local imgBtResultado = canvas:new("../medias/legenda_amarelo_30.png");
  canvas:compose(marginLeft, marginTop, imgBtResultado);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btResultado);

  marginTop = marginTop + 10 + alturaBts;
  local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
  canvas:compose(marginLeft, marginTop, imgBtSair);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
  -- Regiao Botoes (Fim)

  -- Flush
  canvas:flush();
end
function deslocarEscudo(marginLeft, marginTop, imgTime, finalPosicaoEscudo)
  canvas:attrColor(0,0,255,255); -- BLUE
  print(marginLeft, marginTop, imgTime, finalPosicaoEscudo)
  local posicaoX = marginLeft;
  while (posicaoX < finalPosicaoEscudo) do
    canvas:drawRect("fill",0,marginTop,400,50);
    canvas:compose (posicaoX,marginTop,imgTime);
    posicaoX = posicaoX + 1;
    canvas:flush();
  end
end

function getTabelaArquivo(nomeArquivo)
  local tabela = {}
  local arquivo = assert(io.open(nomeArquivo, "r"))
  for linha in arquivo:lines() do
    tabela[#tabela+1] = trim(linha);
  end
  arquivo:close()
  return tabela;
end

function getTabelaArquivoNuvem(linkArquivo)
  dofile("buscarDetalhes.lua")
  local tabelaArquivo = {}
  print("entrou na funcao de pegar arquivo na net")
  local callback = function(tabela,status)
    if(status == 1) then
      tabelaArquivo = tabela;
      desenharDetalhes(tabelaArquivo, status)
    elseif(status == -1) then
      tabelaArquivo = getTabelaArquivo(arquivoTimeGlobal);
      desenharDetalhes(tabelaArquivo, status)
    end
  end
  buscarDetalhes(linkArquivo,callback)
end

function trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function desenharConfrontos(tvTituloPagina, tvPlacar, tvPlacar2)
  -- Strings (Inicio)
  local btDetalhes = "Detalhes";
  local btResultado = "Resultado";
  local btSair = "Sair";
  -- Strings (Fim)

  -- Doubles (Inicio) --
  local marginTop = 10;
  local marginLeft = 10;
  local alturaBts = 30;
  local larguraBts = 30;
  local larguraImgTime = 45;
  local alturaImgTime = 45;
  -- Doubles (Fim)

  -- Regi�o T�tulo (Inicio) --
  canvas:attrFont("Tiresias",48,"bold");

  canvas:attrColor(0,0,255,255); -- BLUE
  canvas:drawRect("fill",0,0,canvas:attrSize());

  canvas:attrColor(0,0,0,255); -- BLACK
  canvas:drawText(marginLeft,0,tvTituloPagina);
  -- Regiao Titulo (Fim)

  -- Regiao Placar (Inicio)
  local larguraTituloPagina,alturaTituloPagina = canvas:measureText(tvTituloPagina);
  marginTop = alturaTituloPagina + marginTop

  canvas:attrFont("Tiresias",24,"bold");
  canvas:drawText(marginLeft + larguraImgTime,marginTop + 5,tvPlacar);
  local larguraTvPlacar, alturaTvPlacar = canvas:measureText(tvPlacar);
  marginTop = alturaImgTime + 15 + marginTop;
  canvas:drawText(marginLeft + larguraImgTime,marginTop + 5,tvPlacar2);
  -- Regiao Placar (Fim)

  -- Regiao Botoes (Inicio)
  marginTop = marginTop + alturaImgTime + 50;

  local imgBtDetalhes = canvas:new("../medias/legenda_ok_30.png");
  canvas:compose(marginLeft, marginTop, imgBtDetalhes);
  canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btDetalhes);

  marginTop = marginTop + 10 + alturaBts;
  local imgBtResultado = canvas:new("../medias/legenda_amarelo_30.png");
  canvas:compose(marginLeft, marginTop, imgBtResultado);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btResultado);

  marginTop = marginTop + 10 + alturaBts;
  local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
  canvas:compose(marginLeft, marginTop, imgBtSair);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
  -- Regiao Botoes (Fim)

  -- Flush
  canvas:flush();
end
