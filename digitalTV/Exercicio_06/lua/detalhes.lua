-- Import (Inicio)
require 'utils';
-- Import (Fim)

function handler(evt)
  if(evt.type=="attribution") then
    if(evt.value=="timeSport") then
      desenharDetalhes("Sport","../medias/times/sport_45.png",'../arquivos/detalhesSport.txt', 'unidades.ufrpe.br/sites/unidades.ufrpe.br/files/detalhesSport.xml');
    elseif(evt.value=="timeSantos") then
      desenharDetalhes("Santos","../medias/times/santos_45x45.png",'../arquivos/detalhesSantos.txt', 'unidades.ufrpe.br/sites/unidades.ufrpe.br/files/detalhesSantos.xml');
    elseif(evt.value=="timeFlamengo") then
      desenharDetalhes("Flamengo","../medias/times/flamengo_45x45.png",'../arquivos/detalhesFlamengo.txt', 'unidades.ufrpe.br/sites/unidades.ufrpe.br/files/detalhesFlamengo.xml');
    elseif(evt.value=="timeVasco") then
      desenharDetalhes("Vasco","../medias/times/vasco_45x45.png",'../arquivos/detalhesVasco.txt', 'unidades.ufrpe.br/sites/unidades.ufrpe.br/files/detalhesVasco.xml');
    end
  end
end

event.register(handler,"ncl")
