<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="conectores" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<connectorBase>
			<causalConnector id="onBeginParar">
				<simpleCondition role="onBegin"/>
				<compoundAction operator="par">
					<simpleAction role="stop" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="naSelecaoPararIniciar">
				<simpleCondition role="onSelection"/>
				<compoundAction operator="seq">
					<simpleAction role="stop" max="unbounded" qualifier="par"/>
					<simpleAction role="start" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="naSelecaoSetarPararIniciar">
				<connectorParam name="var"/>
				<simpleCondition role="onSelection"/>
				<compoundAction operator="seq">
					<simpleAction role="stop" max="unbounded" qualifier="par"/>
					<simpleAction role="start" max="unbounded" qualifier="par"/>
					<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="naSelecaoDeTeclaPararIniciar">
				<connectorParam name="tecla"/>
				<simpleCondition role="onSelection" key="$tecla"/>
				<compoundAction operator="seq">
					<simpleAction role="stop" max="unbounded" qualifier="par"/>
					<simpleAction role="start" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="naSelecaoDeTeclaSetarPararIniciar">
				<connectorParam name="var"/>
				<connectorParam name="tecla"/>
				<simpleCondition role="onSelection" key="$tecla"/>
				<compoundAction operator="seq">
					<simpleAction role="stop" max="unbounded" qualifier="par"/>
					<simpleAction role="start" max="unbounded" qualifier="par"/>
					<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="naSelecaoDeTeclaSetar">
				<connectorParam name="var"/>
				<connectorParam name="tecla"/>
				<simpleCondition role="onSelection" key="$tecla"/>
				<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
			</causalConnector>
			<causalConnector id="naSelecaoDeTeclaIniciar">
				<connectorParam name="tecla"/>
				<simpleCondition role="onSelection" key="$tecla"/>
				<simpleAction role="start" max="unbounded" qualifier="par"/>
			</causalConnector>
			<causalConnector id="naSelecaoDeTeclaParar">
				<connectorParam name="tecla"/>
				<simpleCondition role="onSelection" key="$tecla"/>
				<simpleAction role="stop" max="unbounded" qualifier="par"/>
			</causalConnector>
			<causalConnector id="naSelecaoSetarIniciar">
				<connectorParam name="var"/>
				<simpleCondition role="onSelection"/>
				<compoundAction operator="seq">
					<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
					<simpleAction role="start" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
			<causalConnector id="onEndAttributionStopStart">
				<connectorParam name="var"/>
				<compoundCondition operator="and">
					<simpleCondition role="onEndAttribution"/>
					<assessmentStatement comparator="eq">
						<attributeAssessment role="prop" eventType="attribution"/>
						<valueAssessment value="$var"/>
					</assessmentStatement>
				</compoundCondition>
				<compoundAction operator="seq">
					<simpleAction role="stop" max="unbounded"/>
					<simpleAction role="start" max="unbounded"/>
					<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
				</compoundAction>
			</causalConnector>
		</connectorBase>
	</head>
</ncl>
