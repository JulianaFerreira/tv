-- Import (Inicio) --
require 'utils';
-- Import (Fim) --
local tvTituloPagina = "Rodada 1";
local tvPlacar = "1X1";
local tvPlacar2 = "2X2";

desenharConfrontos(tvTituloPagina, tvPlacar, tvPlacar2);
function handler(evt)
  print(evt.value)
  if(evt.type=="attribution") then

    if(evt.value=="2") then
      tvTituloPagina = "Rodada 2";
      tvPlacar = "4X1";
      tvPlacar2 = "3X2";
      desenharConfrontos(tvTituloPagina, tvPlacar, tvPlacar2);
    elseif(evt.value=="1") then
      tvTituloPagina = "Rodada 1";
      tvPlacar = "1X1";
      tvPlacar2 = "2X2";
      desenharConfrontos(tvTituloPagina, tvPlacar, tvPlacar2);
    end
  end
end

event.register(handler,"ncl")
