-- Import (Inicio)
require 'utils';
-- Import (Fim)

function handler(evt)
  if(evt.type=="attribution") then
    if(evt.value=="timeSport") then
      desenharDetalhes("Sport","../medias/times/sport_45.png",'../arquivos/detalhesSport.txt');
    elseif(evt.value=="timeSantos") then
      desenharDetalhes("Santos","../medias/times/santos_45x45.png",'../arquivos/detalhesSantos.txt');
    elseif(evt.value=="timeFlamengo") then
      desenharDetalhes("Flamengo","../medias/times/flamengo_45x45.png",'../arquivos/detalhesFlamengo.txt');
    elseif(evt.value=="timeVasco") then
      desenharDetalhes("Vasco","../medias/times/vasco_45x45.png",'../arquivos/detalhesVasco.txt');
    end
  end
end

event.register(handler,"ncl")
