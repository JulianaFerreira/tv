-- Doubles (Inicio) --
local marginTop = 5;
local marginLeft = 10;
-- Doubles (Fim)

-- Strings (Inicio)
local placeholder = "Digite a rodada...";
-- Strings (Fim)

local TEXT = ''
local CHAR = ''
local KEY, IDX = nil, -1
local MAP = {
	  ['1'] = { '1' }
	, ['2'] = { '2' }
	, ['3'] = { '3' }
	, ['4'] = { '4' }
	, ['5'] = { '5' }
	, ['6'] = { '6' }
	, ['7'] = { '7' }
	, ['8'] = { '8' }
	, ['9'] = { '9' }
	, ['0'] = { '0' }
}

local UPPER = false
local case = function (c)
	return (UPPER and string.upper(c)) or c
end

local dx, dy = canvas:attrSize()
canvas:attrFont('Tiresias', 16,'bold')

function redraw()
	canvas:attrColor('white')
	canvas:drawRect('fill', 0,0, dx,100)
  if(TEXT == '') then
    print("entraAqui")
    canvas:attrColor(211,211,211,255); -- Gray
    canvas:drawText(marginLeft,marginTop,placeholder);
  else
	 canvas:attrColor('black')
	 canvas:drawText(marginLeft,marginTop, TEXT..case(CHAR)..'|')
  end
	canvas:flush()
end
redraw();

local evt = {
    class = 'ncl',
    type  = 'attribution',
    name  = 'text'
}

local evtRodada = {
    class = 'ncl',
    type  = 'attribution',
    name  = 'text'
}

local function setText (new, outside)
	TEXT = new or TEXT..case(CHAR)
	print("Texto "..TEXT)
	CHAR, UPPER = '', false
	KEY, IDX = nil, -1

	-- notifica o documento NCL
	if not outside then
	  print("not outside")
		evt.value = TEXT
		evt.action = 'start'; event.post(evt)
		evt.action = 'stop';  event.post(evt)
	end
end

local function imprimirHandler (evt)
  if evt.type == 'attribution' then
    print(evtRodada.value)
  end
end
      

local function keyHandler (evt)
	if evt.class ~= 'key' then return end
	if evt.type ~= 'press' then return true end
	local key = evt.key
	-- BACKSPACE
	if (key == 'CURSOR_LEFT') then
		setText( (KEY and TEXT) or string.sub(TEXT, 1, -2) )
	-- UPPER
	elseif (key == 'CURSOR_UP') then
		UPPER = not UPPER
	-- SPACE
	elseif (key == 'CURSOR_RIGHT') then
		setText( (not KEY) and (TEXT..' ') )
	elseif (key == 'ENTER') then
	   print("Clicou em Enter!")
	   evtRodada.value = TEXT
     evtRodada.action = 'start'; 
     event.post(evtRodada);
     evtRodada.action = 'stop';
     event.post(evtRodada);
	   -- Ve qual a rodada e fazer a "navegacao"
	-- NUMBER   
	elseif tonumber(key) then		
		IDX = (IDX + 1) % #MAP[key]
		CHAR = MAP[key][IDX+1]
		KEY = key
		setText()
	end
	redraw()
	return true
end
event.register(keyHandler)
event.register(imprimirHandler)