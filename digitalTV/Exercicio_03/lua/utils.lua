function desenharTabelaClassificacao(numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna)

  -- cor da tabela
  canvas:attrColor(0,255,255,255);
  canvas:drawRect("fill", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);

  -- cor das linhas da tabela
  canvas:attrColor(255,255,255,255); -- WHITE
  canvas:drawRect("frame", marginLeftColuna, marginTopColuna, tamanhoColuna*numeroColunas, tamanhoColuna*numeroLinhas);
  for i=1,numeroLinhas,1 do
    -- linha horizontal
    canvas:drawLine(marginLeftColuna, marginTopColuna + tamanhoColuna*i, marginLeftColuna + numeroColunas*tamanhoColuna, marginTopColuna+tamanhoColuna*i);
  end

  for i=1, numeroColunas,1 do
    -- linha vertical
    canvas:drawLine(marginLeftColuna + tamanhoColuna*i, marginTopColuna, marginLeftColuna + tamanhoColuna*i, marginTopColuna + numeroLinhas * tamanhoColuna);
  end
end

function preencherTabelaClassificacao(tableClassificacao,numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna, paddingLeft)

  canvas:attrColor(0,0,0,255);
  canvas:attrFont("Tiresias",20,"bold");

  for i=1,numeroColunas,1 do
    canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*(i-1), marginTopColuna+paddingLeft, tableClassificacao[1][i]);
  end

  for i=2, numeroLinhas,1 do
    canvas:compose (marginLeftColuna+paddingLeft, marginTopColuna+paddingLeft+tamanhoColuna*(i-1), canvas:new(tableClassificacao[i][1]));
    for j=1, numeroColunas-1,1 do
      canvas:drawText(marginLeftColuna+paddingLeft+tamanhoColuna*j,marginTopColuna+paddingLeft+tamanhoColuna*(i-1),tableClassificacao[i][j+1]);
    end 
  end
end

function desenharDetalhes(nomeTime,srcImgTime)
  -- Strings (Inicio)
  local lblEstadio = "Est�dio: ";
  local lblElenco = "Elenco: ";
  local lblTitulos = "T�tulos: ";
  local btConfrontos = "Confrontos";
  local btResultado = "Resultado";
  local btSair = "Sair";
  local tvTituloPagina = "Detalhes";
  local tvEstadio = "Est�dio "..nomeTime;
  local tvCapacidade = "(capacidade)"
  local tvElenco = "Lista de jogadores do " ..nomeTime;
  local tvTitulos = "Lista de t�tulos do " ..nomeTime;
  -- Strings (Fim)
  
  -- Doubles (Inicio) --
  local marginTop = 10;
  local marginLeft = 10;
  local alturaBts = 30;
  local larguraBts = 30;
  -- Doubles (Fim)
  
  -- Tabelas (Inicio) --
  local tableRegiaoDetalhes = {
    lblEstadio..tvEstadio,
    tvCapacidade,
    lblElenco..tvElenco,
    lblTitulos..tvTitulos};
  -- Tabelas (Fim) --
  
  -- Regi�o T�tulo (Inicio) --
  canvas:attrFont("Tiresias",48,"bold");
  
  canvas:attrColor(0,0,255,255); -- BLUE
  canvas:drawRect("fill",0,0,canvas:attrSize());
  
  canvas:attrColor(0,0,0,255); -- BLACK
  canvas:drawText(marginLeft,0,tvTituloPagina);
  
  local larguraTituloPagina,alturaTituloPagina = canvas:measureText(tvTituloPagina);
  local imgTime = canvas:new(srcImgTime);
  
  marginTop = alturaTituloPagina + marginTop
  canvas:compose (marginLeft,marginTop,imgTime);
  local larguraimgTime,alturaimgTime = imgTime:attrSize();
  
  -- Regiao Titulo (Fim)
  
  -- Regiao Detalhes (Inicio)
  canvas:attrFont("Tiresias",18,"bold");
  local larguraTvEstadio, alturaTvEstadio = canvas:measureText(tvEstadio);
  
  marginTop = marginTop + alturaimgTime + 10;
  for index,value in pairs(tableRegiaoDetalhes) do
    canvas:drawText(marginLeft,marginTop,tostring(value));
    marginTop = marginTop + alturaTvEstadio;
  end
  -- Regiao Detalhes (Fim)
  
  -- Regiao Botoes (Inicio)
  marginTop = marginTop + 50;
  
  local imgBtConfrontos = canvas:new("../medias/legenda_azul_30.png");
  canvas:compose(marginLeft, marginTop, imgBtConfrontos);
  canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btConfrontos);
  
  marginTop = marginTop + 10 + alturaBts;
  local imgBtResultado = canvas:new("../medias/legenda_amarelo_30.png");
  canvas:compose(marginLeft, marginTop, imgBtResultado);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btResultado);
  
  marginTop = marginTop + 10 + alturaBts;
  local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
  canvas:compose(marginLeft, marginTop, imgBtSair);
  canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
  -- Regiao Botoes (Fim)
  
  -- Flush
  canvas:flush();
end
