-- Import (Inicio)
require 'utils';
-- Import (Fim)

-- Strings (Inicio)
local btDetalhes = "Detalhes";
local btResultado = "Resultado";
local btConfrontos = "Confrontos";
local btSair = "Sair";
local tvTituloPagina = "Classifica��o";
local tvPlacar = "1X1";

-- Strings (Fim)

-- Doubles (Inicio) --
local marginTop = 10;
local marginLeft = 10;
local alturaBts = 30;
local larguraBts = 30;
-- Doubles (Fim)

-- Tabelas (Inicio) --

local tableClassificacao = {};

local tableHeader = {
  "Time",
  "J",
  "PTS",
  "SG"
};

local tableLinha1 = {
  "../medias/times/atl_go_45x45.png",
  "3",
  "8",
  "5"
};

local tableLinha2 = {
  "../medias/times/ponte_preta_45.png",
  "3",
  "7",
  "5"
};

local tableLinha3 = {
  "../medias/times/portuguesa_sp45.png",
  "3",
  "6",
  "5"
};

local tableLinha4 = {
  "../medias/times/vasco_45x45.png",
  "3",
  "5",
  "5"
};

table.insert(tableClassificacao, tableHeader);
table.insert(tableClassificacao, tableLinha1);
table.insert(tableClassificacao, tableLinha2);
table.insert(tableClassificacao, tableLinha3);
table.insert(tableClassificacao, tableLinha4);
-- Tabelas (Fim) --

-- Regi�o T�tulo (Inicio) --
canvas:attrFont("Tiresias",48,"bold");

canvas:attrColor(0,0,255,255); -- BLUE
canvas:drawRect("fill",0,0,canvas:attrSize());

canvas:attrColor(0,0,0,255); -- BLACK
canvas:drawText(marginLeft,0,tvTituloPagina);
-- Regiao Titulo (Fim)

-- Regiao Classificacao (Inicio)

local numeroColunas = 4;
local numeroLinhas = 5;
local tamanhoColuna = 60;
local marginTopColuna = 60;
local marginLeftColuna = 20
local paddingLeft = 10;
local alturaTable = numeroLinhas * tamanhoColuna;
 
-- tentativa desenhando tabela --

desenharTabelaClassificacao(numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna);

-- tentativa preenchendo tabela --

preencherTabelaClassificacao(tableClassificacao,numeroColunas, numeroLinhas, tamanhoColuna, marginTopColuna, marginLeftColuna, paddingLeft);

-- Regiao Botoes (Inicio)
marginTop = marginTop + alturaTable + 70;

local imgBtConfrontos = canvas:new("../medias/legenda_azul_30.png");
canvas:compose(marginLeft, marginTop, imgBtConfrontos);
canvas:drawText(marginLeft + larguraBts + 5 , marginTop, btConfrontos);

marginTop = marginTop + 10 + alturaBts;
local imgBtSair = canvas:new("../medias/legenda_vermelho_30.png");
canvas:compose(marginLeft, marginTop, imgBtSair);
canvas:drawText(marginLeft + larguraBts + 5, marginTop, btSair);
-- Regiao Botoes (Fim)

-- Flush
canvas:flush();

function handler(evt)
  if(evt.key == "BLUE")then
    --Tela de resultados
  elseif(evt.key == "RED")then
    --Sair
  else 
    --NADA
  end

end

event.register(handler,"key");
