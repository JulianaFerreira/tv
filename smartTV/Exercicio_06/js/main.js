var teamList;
var time1, time2, time3, time4;

// Initialize function
var init = function() {
	// TODO:: Do your initialization job
	console.log('init() called');

	tizen.tvinputdevice.registerKey("0");
	tizen.tvinputdevice.registerKey("1");
	tizen.tvinputdevice.registerKey("2");
	tizen.tvinputdevice.registerKey("3");
	tizen.tvinputdevice.registerKey("4");
	tizen.tvinputdevice.registerKey("5");
	tizen.tvinputdevice.registerKey("6");
	tizen.tvinputdevice.registerKey("7");
	tizen.tvinputdevice.registerKey("8");
	tizen.tvinputdevice.registerKey("9");

	tizen.tvinputdevice.registerKey("ColorF0Red");
	tizen.tvinputdevice.registerKey("ColorF1Green");
	tizen.tvinputdevice.registerKey("ColorF2Yellow");
	tizen.tvinputdevice.registerKey("ColorF3Blue");

	writeFile(
			"documents",
			"json",
			"teams.json",
			"[{ \"name\":\"Sao Paulo Futebol Clube\", \"shieldSrc\" : \"images/times/sao_paulo_45x45.png\", \"stadiumStatus\" : { \"stadiumName\" : \"Estádio do Morumbi\", \"stadiumCapacity\" : 72.809}, \"players\" : [\"Christian Cueva\",\"Hernanes\",\"Rodrigo Caio\",\"Diego Lugano\",\"Sidney Aparecido Ramos da Silva\",\"Edimar Curitiba Fraga\",\"Lucas Pratto\",\"Julio Buffarini\",\"Marcos Guilherme\",\"Gilberto Oliveira Souza Junior\",\"Denis César de Matos\",\"Petros\",\"Robert Arboleda\",\"Lucas Fernandes da Silva\",\"Maicosuel\",\"Jucilei\",\"Aderlan Santos\",\"Júnior Tavares\",\"Renan Ribeiro\",\"Wellington Nem\",\"Bruno Alves\",\"Jonatan Gómez\",\"Shaylon\",\"Marcio Antonio de Souza Júnior\",\"Éder Militão\",\"Felipe Araruna\",\"Andrew Eric Feitosa\",\"Antonio Thomaz Santos de Barros\",\"Lucas Perri\",\"Pedro Bortoluzo\",\"Bruno Vieira\",\"Denílson Pereira Júnior\",\"Leonardo Natel\",\"Brenner da Silva\"], \"titles\": [\"Copa do Mundo de Clubes da FIFA\",\"Copa Intercontinental[8][9]\",\"Copa Libertadores da América\",\"Copa Sul-Americana\",\"Recopa Sul-Americana\",\"Supercopa Sul-Americana\",\"Copa Conmebol\",\"Copa Master da Conmebol\",\"Campeonato Brasileiro\",\"Torneio Rio-São Paulo\",\"Taça dos Campeões Rio-São Paulo\",\"Campeonato Paulista\",\"Supercampeonato Paulista\"]},{ \"name\":\"Santos Futebol Clube\", \"shieldSrc\" : \"images/times/santos_45x45.png\", \"stadiumStatus\" : { \"stadiumName\" : \"Estádio Urbano Caldeira\", \"stadiumCapacity\" : 16.068}, \"players\" : [\"Bruno Henrique\",\"Ricardo Oliveira\",\"Nilmar\",\"José Carlos Cracco Neto\",\"Vanderlei Farias da Silva\",\"Jonathan Copete\",\"Vitor Bueno\",\"David Braz\",\"Kayke Moreno de Andrade Rodrigues\",\"Victor Ferraz\",\"Renato Dirnei Florêncio\",\"Jean Mota Oliveira de Souza\",\"Rodrigão\",\"Gustavo Henrique Vernes\",\"Matheus Jesus\",\"Emiliano Vecchio\",\"Arthur Gomes\",\"Lucas Veríssimo\",\"Vladimir Hernández\",\"Alison Lopes Ferreira\",\"Thiago Ribeiro\",\"Daniel Guedes\",\"Vladimir Orlando Cardoso de Araújo Filho\",\"Serginho\",\"Yuri Oliveira Lima\",\"Leandro Donizete\",\"Matheus Antunes Ribeiro\",\"Léo Cittadini\",\"Lucas Crispim\",\"Luis Felipe Nascimento dos Santos\",\"Matheus Oliveira Santos\",\"John\",\"João Paulo\",\"Fabián Noguera\",\"Rodrygo Silva de Goes\"], \"titles\": [\"Copa Intercontinental\",\"Recopa Intercontinental\",\"Copa Libertadores da América\",\"Recopa Sul-Americana\",\"Copa Conmebol\",\"Supercopa Sulamericana\",\"Campeonato Brasileiro\",\"Copa do Brasil\",\"Torneio Rio-São Paulo\",\"Taça dos Campeões Estaduais\",\"Campeonato Paulista\",\"Copa Paulista\"]},{ \"name\":\"Clube Náutico Capibaribe\", \"shieldSrc\" : \"images/times/nautico_45.png\", \"stadiumStatus\" : { \"stadiumName\" : \"Arena de Pernambuco\", \"stadiumCapacity\" : 46.154}, \"players\" : [\"Maylson\",\"Rodrigo Souza\",\"Bérgson Gustavo Silveira da Silva\",\"Giva\",\"Bruno da Mota\",\"William Júnior Salles de Lima Souza\",\"Joazi Oliveira da Silva\",\"Tiago dos Santos Alves\",\"Gastón Filgueira\",\"Luiz Alberto da Silva Oliveira\",\"Darlan\",\"Dakson da Silva\",\"Walber\",\"Rodrigo Pereira Possebon\",\"Renan Paulino de Souza\",\"Jean de Oliveira da Rolt\",\"Josimar Rodrigues Souza Roberto\",\"Ronny Pinto\",\"Rodrigo Careca\",\"Paulo Morais de Araújo Júnior\",\"Douglas Vieira\",\"Luizinho Mello\",\"Raimundo José Cutrim\",\"Guilherme\",\"Esquerdinha\",\"Dasaev Matheus Ferreira de Freitas\",\"Hilton Conceição de Sousa\",\"João Ananias Jordão Júnior\",\"Leonardo Vinicius Pereira Luiz\",\"Vitor Michels\",\"Dê\",\"Edvânio dos Reis\",\"Marcelo Oliveira da Silva\",\"Jefferson Adelino dos Santos\",\"João Paulo Pereira da Silva\",\"Bernardo\",\"David\",\"Jackson de Melo Silva\"], \"titles\": [\"Taça Brasil — Zona Norte–Nordeste\",\"Taça Brasil — Zona Norte\",\"Copa dos Campeões do Norte\",\"Torneio dos Campeões do Norte-Nordeste\",\"Campeonato Pernambucano\",\"Torneio Início de Pernambuco\",\"Copa Pernambuco\"]},{ \"name\":\"Sport Club do Recife\", \"shieldSrc\":\"images/times/sport_45.png\", \"stadiumStatus\" : { \"stadiumName\" : \"Estádio Adelmar da Costa Carvalho\", \"stadiumCapacity\" : 35.020}, \"players\" : [\"Diego Souza\",\"André Felipe Ribeiro de Souza\",\"Alessandro Beti Rosa\",\"Rithely\",\"Everton Felipe\",\"Durval\",\"Sander Henrique Bortolotto\",\"Eugenio Mena\",\"Patrick\",\"Ronaldo Alves\",\"Marcos Antônio da Silva Gonçalves\",\"Reinaldo Lenis Montes\",\"Samuel Xavier Brito\",\"Oswaldo José Henríquez Bocanegra\",\"José Rogério de Oliveira Melo\",\"Wesley Lopes Beltrame\",\"Leandro\",\"Osvaldo Filho\",\"Agenor\",\"Raul Prata\",\"Anselmo de Moraes\",\"Thomás Jaguaribe Bedinelli\",\"Thallyson\",\"Bruno Xavier\",\"Hueglo dos Santos Neris\",\"Rodrigo Vasconcelos Oliveira\",\"Igor Ribeiro\",\"Patrick Correia Sales\",\"Evandro Lima de Oliveira\",\"Edimar Ribeiro da Costa Junior\",\"Lucas Ferreira\",\"Mailson\",\"Wildson Silva Melo\"], \"titles\": [\"Tríplice Coroa\",\"Campeonato Brasileiro\",\"Copa do Brasil\",\"Troféu Roberto Gomes Pedrosa\",\"Campeonato Brasileiro - Série B\",\"Torneio Norte–Nordeste\",\"Copa do Nordeste\",\"Campeonato Pernambucano\",\"Copa Pernambuco\",\"Torneio Início\"]}]");

	/*
	 * // Listener para verificar o status da rede
	 * webapis.network.addNetworkStateChangeListener(function(value) {
	 * console.log("teste" + value); if (value ==
	 * webapis.network.NetworkState.GATEWAY_DISCONNECTED) { console.log("Ta com
	 * internet!"); } else if (value ==
	 * webapis.network.NetworkState.GATEWAY_CONNECTED) { console.log("Ta sem
	 * internet!"); } });
	 */

	inicializarConfrontos();
	carregarJson("http://unidades.ufrpe.br/sites/unidades.ufrpe.br/files/teams.json");

	// add eventListener for keydown
	document.addEventListener('keydown', function(e) {
		console.log('Key code : ' + e.keyCode);
		var html_object = document.activeElement.id;
		switch (e.keyCode) {
		case 37: // LEFT arrow
			if (html_object === "time2") {
				document.getElementById("time1").focus();
			} else if (html_object === "time4") {
				document.getElementById("time3").focus();
			}
			break;
		case 38: // UP arrow
			if (html_object === "time3") {
				document.getElementById("time1").focus();
			} else if (html_object === "time4") {
				document.getElementById("time2").focus();
			} else if (html_object === "mySearch") {
				document.getElementById("time3").focus();
			}
			break;
		case 39: // RIGHT arrow
			if (html_object === "time1") {
				document.getElementById("time2").focus();
			} else if (html_object === "time3") {
				document.getElementById("time4").focus();
			}
			break;
		case 40: // DOWN arrow
			if (html_object === "time1") {
				document.getElementById("time3").focus();
			} else if (html_object === "time2") {
				document.getElementById("time4").focus();
			} else {
				document.getElementById("mySearch").focus();
			}
			break;
		case 405: // Yellow
			document.getElementById("rodada").setAttribute("hidden", '');
			document.getElementById("detalhes").setAttribute("hidden", '');
			document.getElementById("resultado").removeAttribute("hidden");
			break;
		case 406: // BLUE
			document.getElementById("resultado").setAttribute("hidden", '');
			document.getElementById("detalhes").setAttribute("hidden", '');
			document.getElementById("rodada").removeAttribute("hidden");
			document.getElementById("time1").focus();
			break;
		case 403: // RED
			tizen.application.getCurrentApplication().exit();
			break;
		case 13: // OK button
			if (html_object == "time1") {
				carregarDetalhes(time1);
			} else if (html_object == "time2") {
				carregarDetalhes(time2);
			} else if (html_object == "time3") {
				carregarDetalhes(time3);
			} else if (html_object == "time4") {
				carregarDetalhes(time4);
			} else {
				var input = document.getElementById('mySearch').value;
				console.log(input);
				if (input == 1) {
					time1 = teamList[0];
					time2 = teamList[1];
					time3 = teamList[2];
					time4 = teamList[3];
					// chama dados da rodada 1
					document.getElementById('titulo').innerHTML = "Rodada 1";
					document.getElementById('placar1').innerHTML = "7x1";
					document.getElementById('placar2').innerHTML = "1x10";
					document.getElementById('time1').src = time1.shieldSrc;
					document.getElementById('time2').src = time2.shieldSrc;
					document.getElementById('time3').src = time3.shieldSrc;
					document.getElementById('time4').src = time4.shieldSrc;
					document.getElementById("time1").focus();

				} else if (input == 2) {
					time1 = teamList[3];
					time2 = teamList[1];
					time3 = teamList[0];
					time4 = teamList[2];
					// chama dados da rodada 2
					document.getElementById('titulo').innerHTML = "Rodada 2";
					document.getElementById('placar1').innerHTML = "3x3";
					document.getElementById('placar2').innerHTML = "4x4";
					document.getElementById('time1').src = time1.shieldSrc;
					document.getElementById('time2').src = time2.shieldSrc;
					document.getElementById('time3').src = time3.shieldSrc;
					document.getElementById('time4').src = time4.shieldSrc;
					document.getElementById("time1").focus();
				}
			}
			break;
		case 10009: // RETURN button
			break;
		default:
			console.log('Key code : ' + e.keyCode);
			break;
		}
	});
};
window.onload = init;

function readFile(dirRes, filePath) {
	var text = "";
	tizen.filesystem.resolve(dirRes, function(dir) {
		var file = dir.resolve(filePath);
		file.openStream("r", function(fs) {
			text = fs.read(file.fileSize);
			carregarColecaoTime(text);
			alert("Obs: Os dados dos times podem estar desatualizados!");
			fs.close();
		}, function(e) {
			console.log("Error " + e.message);
		}, "UTF-8");
	});
	return text;
}

function writeFile(dirRes, fileDir, fileName, content) {
	console.log("Writing file");
	tizen.filesystem.resolve(dirRes, function(dir) {
		console.log(dir + "/" + fileName);
		var newDir = dir.createDirectory(fileDir);
		var newFile = newDir.createFile(fileName);
		newFile.openStream("w", function(fs) {
			console.log(content);
			fs.write(content);
			fs.close();
		}, function(e) {
			console.log("Error " + e.message);
		}, "UTF-8");
	});
}

function carregarDetalhes(timeJson) {
	document.getElementById('estadio').innerHTML = timeJson.stadiumStatus.stadiumName;
	document.getElementById('elenco').innerHTML = timeJson.players[0];
	document.getElementById('titulos').innerHTML = timeJson.titles[0];
	document.getElementById("time").src = timeJson.shieldSrc;

	document.getElementById("rodada").setAttribute("hidden", '');
	document.getElementById("resultado").setAttribute("hidden", '');
	document.getElementById("detalhes").removeAttribute("hidden");
}

/*
 * function testConnection(url) { var xmlhttp = new XMLHttpRequest();
 * xmlhttp.onload = function() {
 * loadJson("http://unidades.ufrpe.br/sites/unidades.ufrpe.br/files/teams.json"); };
 * xmlhttp.onerror = function() { readFile("documents", "json/teams.json"); };
 * xmlhttp.open("GET", url, true); xmlhttp.send(); }
 */
function carregarJson(url) {
	var xhttp;
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) {
			carregarColecaoTime(this.responseText);
		} else if (this.readyState === 4 && (this.status === 404 || this.status === 503)) {
			readFile("documents", "json/teams.json");
		}
	};

	xhttp.timeout = 2000; // 2s
	xhttp.ontimeout = function() {
		readFile("documents", "json/teams.json");
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

function inicializarConfrontos() {
	document.getElementById("detalhes").setAttribute("hidden", '');
	document.getElementById("resultado").setAttribute("hidden", '');
	document.getElementById('titulo').innerHTML = "Rodada 1";
	document.getElementById('placar1').innerHTML = "7x1";
	document.getElementById('placar2').innerHTML = "1x10";
	document.getElementById('time1').src = "images/times/sao_paulo_45x45.png";
	document.getElementById('time2').src = "images/times/santos_45x45.png";
	document.getElementById('time3').src = "images/times/nautico_45.png";
	document.getElementById('time4').src = "images/times/sport_45.png";
	document.getElementById("time1").focus();

}

function carregarColecaoTime(text) {
	teamList = JSON.parse(text);
	time1 = teamList[0];
	time2 = teamList[1];
	time3 = teamList[2];
	time4 = teamList[3];
}