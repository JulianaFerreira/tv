var checkTime;

//Initialize function
var init = function () {
    // TODO:: Do your initialization job
    console.log('init() called');
	
	//Inicia com foco no time1
	document.getElementById("time1").focus();
	document.getElementById("detalhes").setAttribute("hidden",'');
	document.getElementById("resultado").setAttribute("hidden",'');
    
    document.addEventListener('visibilitychange', function() {
        if(document.hidden){
            // Something you want to do when hide or exit.
        } else {
            // Something you want to do when resume.
        }
    });
 
    // add eventListener for keydown
    document.addEventListener('keydown', function(e) {
    	console.log('Key code : ' + e.keyCode);
		var html_object = document.activeElement.id;
    	switch(e.keyCode){
    	case 37: //LEFT arrow
			if(html_object == "time2"){
				document.getElementById("time1").focus();
			} else if(html_object == "time4"){
				document.getElementById("time3").focus();
			}
    		break;
    	case 38: //UP arrow
			if(html_object == "time3"){
				document.getElementById("time1").focus();
			} else if(html_object == "time4"){
				document.getElementById("time2").focus();
			}
    		break;
    	case 39: //RIGHT arrow
			if(html_object == "time1"){
				document.getElementById("time2").focus();
			} else if(html_object == "time3"){
				document.getElementById("time4").focus();
			}
    		break;
    	case 40: //DOWN arrow
			if(html_object == "time1"){
				document.getElementById("time3").focus();
			} else if(html_object == "time2"){
				document.getElementById("time4").focus();
			}
    		break;
		case 405: //Yellow
			document.getElementById("confrontos").setAttribute("hidden",'');	
			document.getElementById("detalhes").setAttribute("hidden",'');
    		document.getElementById("resultado").removeAttribute("hidden");
    		break;
		case 406: //BLUE
			document.getElementById("resultado").setAttribute("hidden",'');	
			document.getElementById("detalhes").setAttribute("hidden",'');
    		document.getElementById("confrontos").removeAttribute("hidden");
			document.getElementById("time1").focus();
    		break;
		case 403: //RED
			tizen.application.getCurrentApplication().exit();
    		break;		
    	case 13: //OK button
			document.getElementById("confrontos").setAttribute("hidden",'');
			var img = document.getElementById('time');
			if(html_object == "time1"){
				img.src = 'images/times/sport_45.png';
				document.getElementById('estadio').innerHTML = "Estádio: Nome do estádio Sport";
				document.getElementById('elenco').innerHTML = "Elenco: Lista de jogadores Sport";
				document.getElementById('titulos').innerHTML = "Títulos: Lista de títulos Sport";
			} else if(html_object == "time2"){
				img.src = 'images/times/nautico_45.png';
				document.getElementById('estadio').innerHTML = "Estádio: Nome do estádio Náutico";
				document.getElementById('elenco').innerHTML = "Elenco: Lista de jogadores Náutico";
				document.getElementById('titulos').innerHTML = "Títulos: Lista de títulos Náutico";
			}else if(html_object == "time3"){
				img.src = 'images/times/santos_45x45.png';
				document.getElementById('estadio').innerHTML = "Estádio: Nome do estádio Santos";
				document.getElementById('elenco').innerHTML = "Elenco: Lista de jogadores Santos";
				document.getElementById('titulos').innerHTML = "Títulos: Lista de títulos Santos";
			}else{
				img.src = 'images/times/sao_paulo_45x45.png';
				document.getElementById('estadio').innerHTML = "Estádio: Nome do estádio São Paulo";
				document.getElementById('elenco').innerHTML = "Elenco: Lista de jogadores São Paulo";
				document.getElementById('titulos').innerHTML = "Títulos: Lista de títulos São Paulo";
			}
    		document.getElementById("detalhes").removeAttribute("hidden");
    		break;
    	case 10009: //RETURN button
			break;
    	default:
    		console.log('Key code : ' + e.keyCode);
    		break;
    	}
    });
};
// window.onload can work without <body onload="">
window.onload = init;

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('divbutton1').innerHTML='Current time: ' + h + ':' + m + ':' + s;
    setTimeout(startTime, 10);
}

function checkTime(i) {
    if (i < 10) {
        i='0' + i;
    }
    return i;
}
