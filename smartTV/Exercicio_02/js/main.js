var checkTime;

//Initialize function
var init = function () {
    // TODO:: Do your initialization job
    console.log('init() called');
	
	//Inicia com foco no time1
	document.getElementById("time1").focus();
    
    document.addEventListener('visibilitychange', function() {
        if(document.hidden){
            // Something you want to do when hide or exit.
        } else {
            // Something you want to do when resume.
        }
    });
 
    // add eventListener for keydown
    document.addEventListener('keydown', function(e) {
		var html_object = document.activeElement.id;
    	switch(e.keyCode){
    	case 37: //LEFT arrow
			if(html_object == "time2"){
				document.getElementById("time2").blur;
				document.getElementById("time1").focus();
			} else if(html_object == "time4"){
				document.getElementById("time4").blur;
				document.getElementById("time3").focus();
			}
    		break;
    	case 38: //UP arrow
			if(html_object == "time3"){
				document.getElementById("time3").blur;
				document.getElementById("time1").focus();
			} else if(html_object == "time4"){
				document.getElementById("time4").blur;
				document.getElementById("time2").focus();
			}
    		break;
    	case 39: //RIGHT arrow
			if(html_object == "time1"){
				document.getElementById("time1").blur;
				document.getElementById("time2").focus();
			} else if(html_object == "time3"){
				document.getElementById("time3").blur;
				document.getElementById("time4").focus();
			}
    		break;
    	case 40: //DOWN arrow
			if(html_object == "time1"){
				document.getElementById("time1").blur;
				document.getElementById("time3").focus();
			} else if(html_object == "time2"){
				document.getElementById("time2").blur;
				document.getElementById("time4").focus();
			}
    		break;
    	case 13: //OK button
    		break;
    	case 10009: //RETURN button
		tizen.application.getCurrentApplication().exit();
    		break;
    	default:
    		console.log('Key code : ' + e.keyCode);
    		break;
    	}
    });
};
// window.onload can work without <body onload="">
window.onload = init;

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('divbutton1').innerHTML='Current time: ' + h + ':' + m + ':' + s;
    setTimeout(startTime, 10);
}

function checkTime(i) {
    if (i < 10) {
        i='0' + i;
    }
    return i;
}
